package todo

import (
	"errors"
	"net/http"
	"strconv"

	"github.com/labstack/echo"
	"github.com/labstack/gommon/log"
	"gitlab.com/incytestudios/golang-boilerplate-web-server/server"
	"gitlab.com/incytestudios/golang-boilerplate-web-server/server/db"
	"gitlab.com/incytestudios/golang-boilerplate-web-server/server/models/todo"
)

func stringToID(in string) (out int, err error) {
	out64, err := strconv.ParseInt(in, 10, 64)
	if err == nil {
		return int(out64), nil
	}
	return
}

// HandleGetTodos returns all todos as JSON
func HandleGetTodos(s *server.Server) echo.HandlerFunc {
	log.Debug("in closure, handle get todos")
	return func(c echo.Context) error {
		todos, err := todo.MGet(s.DB, 0, 100, todo.OrderByDateCreated, db.OrderDescending)
		if err != nil {
			return err
		}
		return c.JSON(http.StatusOK, todos)
	}
}

// HandleGetTodoByID returns a single todo item based on the requested ID
func HandleGetTodoByID(s *server.Server) echo.HandlerFunc {
	log.Debug("in closure, handle get todo by id")
	return func(c echo.Context) error {
		id, err := stringToID(c.Param("id"))
		if err != nil {
			return errors.New("ID must be an integer")
		}
		t, err := todo.GetByID(s.DB, id)
		if err != nil {
			return err
		}
		return c.JSON(http.StatusOK, t)
	}
}

// HandleDeleteTodoByID deleted an individual todo by it's ID
func HandleDeleteTodoByID(s *server.Server) echo.HandlerFunc {
	log.Debug("in closure, handle delete todo by id")
	return func(c echo.Context) error {
		id, err := stringToID(c.Param("id"))
		if err != nil {
			return errors.New("ID must be an integer")
		}
		err = todo.DeleteByID(s.DB, id)
		if err != nil {
			return err
		}
		return c.HTML(http.StatusNoContent, "")
	}
}

// HandleCreateTodo takes POST of new TODOs
func HandleCreateTodo(s *server.Server) echo.HandlerFunc {
	log.Debug("in closure, handle create todo")
	return func(c echo.Context) error {
		dummy := &todo.Todo{}
		if err := c.Bind(dummy); err != nil {
			return err
		}
		t, err := todo.NewTodo(s.DB, dummy.Name)
		if err != nil {
			return err
		}
		return c.JSON(http.StatusCreated, t)
	}
}
