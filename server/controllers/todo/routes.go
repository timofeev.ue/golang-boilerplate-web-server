package todo

import (
	"github.com/labstack/echo"
	"gitlab.com/incytestudios/golang-boilerplate-web-server/server"
)

// InstallOnGroup binds urls to handlers for the todo package
func InstallOnGroup(s *server.Server, g *echo.Group) {
	g.GET("/todos", HandleGetTodos(s))
	g.POST("/todos", HandleCreateTodo(s))
	g.GET("/todos/:id", HandleGetTodoByID(s))
	g.DELETE("/todos/:id", HandleDeleteTodoByID(s))
}
