package status

import (
	"net/http"

	"github.com/labstack/echo"
	"gitlab.com/incytestudios/golang-boilerplate-web-server/server"
)

type statusResult struct {
	IsHealthy         bool
	IsPostgresHealthy bool
}

// HandleStatus should check all required services are up and running. This
// is the health check that load balancers should be hitting. If a required
// connection is unhealthy, we should be removed from the pool
func HandleStatus(s *server.Server) echo.HandlerFunc {
	return func(c echo.Context) error {
		// all health checks will default to false, so just flag them true after
		// proving they're healthy
		var result statusResult

		err := s.DB.Ping()
		result.IsPostgresHealthy = (err == nil)

		// we're only healthy if every other item is healthy, as you add new checks,
		// ensure they are ANDed together below
		result.IsHealthy = result.IsPostgresHealthy
		return c.JSON(http.StatusOK, result)
	}
}

// HandlePing responds with a simple 200 as a fast check the server is listening
func HandlePing(c echo.Context) error {
	return c.HTML(http.StatusOK, "PONG")
}
