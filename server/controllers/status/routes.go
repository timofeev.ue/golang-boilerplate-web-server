package status

import "gitlab.com/incytestudios/golang-boilerplate-web-server/server"

// Install binds routes to handlers for status
func Install(s *server.Server) {
	s.GET("/status", HandleStatus(s))
	s.GET("/ping", HandlePing)
}
