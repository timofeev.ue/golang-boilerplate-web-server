package clientlog

import (
	"fmt"
	"net/http"

	log "github.com/sirupsen/logrus"
	"gitlab.com/incytestudios/golang-boilerplate-web-server/server"

	"github.com/labstack/echo"
)

// ArbitraryJSON will take any strings and map them to interfaces
type ArbitraryJSON map[string]interface{}

// AsFields makes the JSON suitable for logging in logrus.WithFields
func (j ArbitraryJSON) AsFields() log.Fields {
	f := log.Fields{}
	for k, v := range j {
		f[k] = v
	}
	return f
}

// Handler should check all required services are up and running. This
// is the health check that load balancers should be hitting. If a required
// connection is unhealthy, we should be removed from the pool
func Handler(s *server.Server) echo.HandlerFunc {
	return func(c echo.Context) error {
		json := ArbitraryJSON{}
		c.Bind(&json)
		entry := log.WithFields(json.AsFields())

		msg := fmt.Sprintf("FROM CLIENT: %s", json["msg"])
		switch json["level"] {
		case "trace":
			entry.Trace(msg)
		case "debug":
			entry.Debug(msg)
		case "info":
			entry.Info(msg)
		case "warn", "warning":
			entry.Warning(msg)
		case "error", "critical":
			entry.Error(msg)
		default:
			entry.Error(msg)
		}
		return c.JSON(http.StatusAccepted, "")
	}
}
