package clientlog

import "gitlab.com/incytestudios/golang-boilerplate-web-server/server"

// Install binds routes to handlers for clientlog
func Install(s *server.Server) {
	s.POST("/log", Handler(s))
}
