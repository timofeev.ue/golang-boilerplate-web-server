package auth

import (
	"github.com/labstack/echo"
	"gitlab.com/incytestudios/golang-boilerplate-web-server/server"
)

// Install puts todo related routes on the server and specified their handlers
func Install(s *server.Server) {
	s.POST("/login", HandleLogin(s))
}

// InstallOnGroup adds the route to an existing group on the server
func InstallOnGroup(s *server.Server, g *echo.Group) {
	g.GET("/authcheck", HandleCheck(s))
}
