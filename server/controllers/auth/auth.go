package auth

import (
	"net/http"
	"time"

	"gitlab.com/incytestudios/golang-boilerplate-web-server/server"
	"gitlab.com/incytestudios/golang-boilerplate-web-server/server/config"
	"gitlab.com/incytestudios/golang-boilerplate-web-server/server/models/user"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"
	log "github.com/sirupsen/logrus"
)

// HandleLogin looks for basic auth info and if allowed returns
// a JWT suitable for further API use
func HandleLogin(s *server.Server) echo.HandlerFunc {
	return func(c echo.Context) error {
		type credentials struct {
			Email    string `json:"email" form:"email" query:"email"`
			Password string `json:"password" form:"password" query:"password"`
		}
		var creds credentials
		err := c.Bind(&creds)
		if err != nil {
			return err
		}

		u, err := user.Authenticate(s.DB, creds.Email, creds.Password)
		if err != nil {
			// TODO: cast error to differentiate between server errors and bad password
			c.Get("log").(*log.Entry).WithFields(log.Fields{
				"error": err.Error(),
			}).Error("auth error", err)
			return echo.ErrUnauthorized
		}
		claims := &CustomClaims{
			ID:           u.ID,
			Name:         u.Name,
			Email:        u.Email,
			DateCreated:  u.DateCreated,
			DateModified: u.DateModified,
			StandardClaims: jwt.StandardClaims{
				ExpiresAt: time.Now().Add(config.AuthTokenTTL).Unix(),
			},
		}
		token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

		t, err := token.SignedString([]byte(config.JWTSalt))
		if err != nil {
			return err
		}
		return c.JSON(http.StatusOK, echo.Map{
			"token": t,
		})
	}
}

// HandleCheck just validates a client's JWT as cheaply as possible
func HandleCheck(s *server.Server) echo.HandlerFunc {
	return func(c echo.Context) error {
		return c.HTML(http.StatusOK, "OK")
	}
}
