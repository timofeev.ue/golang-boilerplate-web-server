/*Package custommiddleware implements echo middleware

This file was orignally copied from https://github.com/bakatz/echo-logrusmiddleware
which was out of date. Updated it to handle new echo interfaces as well as
making Hook() accept a logger instance to use in the closure (so that loggly
hooks are used)
---- TS (29 Oct 2018 08:50:01 PM)
*/
package custommiddleware

import (
	"strconv"
	"time"

	"github.com/labstack/echo"
	log "github.com/sirupsen/logrus"
)

// logrusMiddlewareHandler has been changed to accept a logrus.Logger as the first argument
// this allows the inner functions to use a Logger that was configured elsewhere (one with
// hooks attached such as Loggly logging)
func logrusMiddlewareHandler(c echo.Context, level int, next echo.HandlerFunc) error {
	if level == 0 {
		return next(c)
	}
	req := c.Request()
	res := c.Response()
	requestID := res.Header().Get(echo.HeaderXRequestID)
	c.Set("request_id", requestID)
	// setup a context logger
	c.Set("log", log.WithField("request_id", requestID))

	start := time.Now()
	if err := next(c); err != nil {
		c.Error(err)
	}
	stop := time.Now()

	bytesIn := req.Header.Get(echo.HeaderContentLength)
	if bytesIn == "" {
		bytesIn = "0"
	}
	fields := map[string]interface{}{
		"method":     req.Method,
		"uri":        req.RequestURI,
		"status":     res.Status,
		"request_id": requestID,
	}
	if level > 1 {
		fields["request_id"] = res.Header().Get(echo.HeaderXRequestID)
		fields["time_rfc3339"] = time.Now().Format(time.RFC3339)
		fields["remote_ip"] = c.RealIP()
		fields["host"] = req.Host
		fields["referer"] = req.Referer()
		fields["user_agent"] = req.UserAgent()
		fields["latency"] = strconv.FormatInt(stop.Sub(start).Nanoseconds()/1000, 10)
		fields["latency_human"] = stop.Sub(start).String()
		fields["bytes_in"] = bytesIn
		fields["bytes_out"] = strconv.FormatInt(res.Size, 10)
	}

	log.WithFields(fields).Info("Handled request")

	return nil
}

// Hook returns echo Middleware that can be "Use()"d by an echo server. It accepts
// a pre-configured Logger (unlike the original version)
func Hook(level int) echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			return logrusMiddlewareHandler(c, level, next)
		}
	}
}
