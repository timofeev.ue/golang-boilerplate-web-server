// +build tools

// Pacakge tools tracks tools used in development that are not used in
// production code so they get picked up by the go module system
package tools

import (
	_ "github.com/joho/godotenv"
)
