package server

import (
	"gitlab.com/incytestudios/golang-boilerplate-web-server/server/config"
	"gitlab.com/incytestudios/golang-boilerplate-web-server/server/middleware"

	"github.com/jmoiron/sqlx"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	log "github.com/sirupsen/logrus"
)

// Server is the API HTTP server (just wraps echo.Echo)
type Server struct {
	*echo.Echo
	DB *sqlx.DB
}

// NewServer creates a new API server with all middleware configured but no
// routes
func NewServer() *Server {
	log.WithFields(log.Fields{
		"DebugLogging": config.DebugLogging,
		"Port":         config.Port,
		"LogglyToken":  config.LogglyToken,
	}).Debug("making server")

	e := echo.New()
	e.HideBanner = true
	e.Debug = config.DebugLogging

	// configure Logrus logging for Echo
	e.Use(middleware.RequestID())
	e.Use(custommiddleware.Hook(config.RequestLoggingLevel))
	e.Use(middleware.Recover()) // https://echo.labstack.com/middleware/recover
	e.Use(middleware.CORS())

	out := &Server{Echo: e}
	return out
}

// Run begins listening on the configured address
func (s *Server) Run() error {
	return s.Start(":" + config.Port)
}
