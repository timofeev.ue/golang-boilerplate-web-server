package db

import (
	"fmt"

	log "github.com/sirupsen/logrus"

	"github.com/jmoiron/sqlx"
	// Import postgres driver for side-effects
	"github.com/lib/pq"
)

// ErrCheck is just shorthand for ErrCheckWithFields when you have no fields
func ErrCheck(err error, extraInfo string) {
	errLog(err, false, extraInfo, log.Fields{})
}

// FatalCheck is just shorthand for FatalCheckWithFields when you have no fields
func FatalCheck(err error, extraInfo string) {
	errLog(err, true, extraInfo, log.Fields{})
}

// ErrCheckWithFields considers logs errors with fields
func ErrCheckWithFields(err error, extraInfo string, fields log.Fields) {
	errLog(err, false, extraInfo, fields)
}

// FatalCheckWithFields considers any non-nil err fatal, and kills the process
// with as much logging information as possible
func FatalCheckWithFields(err error, extraInfo string, fields log.Fields) {
	errLog(err, true, extraInfo, fields)
}

func errLog(err error, fatalOnError bool, extraInfo string, fields log.Fields) {
	if err == nil { // no error don't do anything else
		return
	}
	entry := log.WithFields(fields).WithFields(log.Fields{
		"error": err.Error(),
	})
	if err, ok := err.(*pq.Error); ok {
		// this is a libpq error, so we can probably pull a lot more info out of
		// this error
		entry = entry.WithFields(log.Fields{
			"severity":          err.Severity,
			"code":              err.Code,
			"message":           err.Message,
			"detail":            err.Detail,
			"hint":              err.Hint,
			"position":          err.Position,
			"internal_position": err.InternalPosition,
			"where":             err.Where,
			"schema":            err.Schema,
			"table":             err.Table,
			"column":            err.Column,
			"data_type_name":    err.DataTypeName,
			"constraint":        err.Constraint,
			"file":              err.File,
			"line":              err.Line,
			"routine":           err.Routine,
		})
	}
	if fatalOnError {
		entry.Fatal(extraInfo)
	} else {
		entry.Error(extraInfo)
	}
}

// TruncateTable nukes a table and anything keyed off it
func TruncateTable(d *sqlx.DB, tableName string) {
	query := fmt.Sprintf("TRUNCATE TABLE %s CASCADE", pq.QuoteIdentifier(tableName))
	_, err := d.Exec(query)
	ErrCheckWithFields(err, "failed to truncate table", log.Fields{
		"table": tableName,
	})
}
