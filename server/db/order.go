package db

// OrderDirection is for use with SQL ORDER clause
type OrderDirection int

const (
	// OrderAscending corresponds to SQL's ASCENDING directive
	OrderAscending OrderDirection = iota
	// OrderDescending corresponds to SQL's DESCENDING directive
	OrderDescending
)

// String returns the SQL syntax for an OrderDirection
func (o OrderDirection) String() (out string) {
	switch o {
	case OrderAscending:
		out = "ASC"
	case OrderDescending:
		out = "DESC"
	}
	return
}
