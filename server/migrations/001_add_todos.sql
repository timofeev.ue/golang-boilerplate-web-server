CREATE TABLE todos (
  id SERIAL,
  name text NOT NULL,
  is_done boolean NOT NULL DEFAULT FALSE,
  date_created timestamp without time zone NOT NULL DEFAULT now(),
  date_modified timestamp without time zone NOT NULL DEFAULT now()
);

CREATE TRIGGER sync_date_modified BEFORE UPDATE ON todos
  FOR EACH ROW EXECUTE PROCEDURE sync_date_modified();
