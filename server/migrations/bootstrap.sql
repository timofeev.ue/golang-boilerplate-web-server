/* The very first thing to run on a clean database to enable it for
migrations. Only the bare minimum schema should go in here, the rest
should be done via migrations #-- TS (23 Mar 2015 04:09:40 PM)
*/

-- Template tables and migration tracking will live in the meta schema
CREATE SCHEMA IF NOT EXISTS meta;

CREATE TABLE meta.migrations (
  current_version integer PRIMARY KEY NOT NULL,
  filename varchar(1024),
  date_migrated timestamp without time zone NOT NULL DEFAULT now()
);
INSERT INTO meta.migrations (current_version, filename)
  VALUES (0, 'bootstrap.sql');

-- Generic function that will update the date_modified column on edits
-- to any table with this trigger attached
CREATE OR REPLACE FUNCTION sync_date_modified() RETURNS TRIGGER AS $$
BEGIN
  NEW.date_modified := now();
  RETURN NEW;
END;
$$ LANGUAGE PLPGSQL;
