-- DROP TABLE if exists users;
-- \c goapi - to connect to a pg db
-- \i {this file}.sql

CREATE EXTENSION IF NOT EXISTS pgcrypto;

CREATE TABLE users (
  -- SERIAL auto increments
  id SERIAL,
  email text NOT NULL unique,
  password text NOT NULL,
  name text,
  date_created timestamp without time zone NOT NULL DEFAULT now(),
  date_modified timestamp without time zone NOT NULL DEFAULT now()
);

-- Emails need to be unique
CREATE UNIQUE INDEX lower_case_email ON users ((lower(email)));

CREATE TRIGGER sync_date_modified BEFORE UPDATE ON users
  FOR EACH ROW EXECUTE PROCEDURE sync_date_modified();
