// Package config handles all application configuration. It reads settings
// from the running environment, or for local development it reads the
// contents of a file named `.env` in the project root
package config

import (
	"os"
	"strconv"
	"time"

	// Side-effect import injects contents of .env into running Environment
	_ "github.com/joho/godotenv/autoload"
)

var (
	// Port is the TCP port on which the server will listen
	Port = getEnvDefault("PORT", "5000")
	// DebugLogging if true, will log extra data
	DebugLogging = mustParseBool(getEnvDefault("DEBUG_LOGGING", "false"))
	// RequestLoggingLevel controls how much info is logged per request, it can be
	// set to 0 (no request logging) 1 (route, method, response) 2 (everything)
	RequestLoggingLevel = mustParseInt(getEnvDefault("REQUEST_LOGGING_LEVEL", "2"))
	// LogglyToken if set will send logs to Loggly
	LogglyToken = getEnvDefault("LOGGLY_TOKEN", "")
	// JWTSalt is the private signing key for server-generated JWTs
	JWTSalt = getEnvDefault("JWT_SALT", "CHANGEME")
	// DBSalt is the private used for salting passwords in postgres
	DBSalt = getEnvDefault("DB_SALT", "CHANGEME")
	// AuthTokenTTL is the time-to-live of a JWT
	AuthTokenTTL = mustParseDuration(getEnvDefault("AUTH_TOKEN_TTL", "24h"))
	// DatabaseURL is the fully qualified user/pass/host/port/db of a postgres
	// instance
	DatabaseURL = getEnvDefault("DATABASE_URL", "")
	// DatabaseTestURL is the fully qualified user/pass/host/port/db of a postgres
	// instance that will be used for unit testing
	DatabaseTestURL = getEnvDefault("DATABASE_TEST_URL", "")
)

// Parse a boolean value for `val` or panic
func mustParseBool(val string) bool {
	b, err := strconv.ParseBool(val)
	if err != nil {
		panic(err)
	}
	return b
}

// Parse an int value for `val` or panice
func mustParseDuration(val string) time.Duration {
	d, err := time.ParseDuration(val)
	if err != nil {
		panic(err)
	}
	return d
}

func mustParseInt(val string) int {
	i, err := strconv.ParseInt(val, 10, 64)
	if err != nil {
		panic(err)
	}
	return int(i)
}

// getEnvDefault returns the value of environment variable key or
// defaulValue if the env variable doesn't exist
func getEnvDefault(key string, defaultValue string) string {
	val := os.Getenv(key)
	if val == "" {
		return defaultValue
	}
	return val
}
