package user

import (
	"os"
	"testing"

	"gitlab.com/incytestudios/golang-boilerplate-web-server/server/db"
)

// TestMain creates some dummy users for other tests and wipes the users table
// before exit
func TestMain(m *testing.M) {
	d := db.MustGetTestDB()
	db.TruncateTable(d, "users")       // before we start
	defer db.TruncateTable(d, "users") // and again when done

	var err error
	_, err = CreateUser(d, "user1", "user1@u.com", "pass")
	db.ErrCheck(err, "couldn't make user1")
	_, err = CreateUser(d, "user2", "user2@u.com", "pass2")
	db.ErrCheck(err, "couldn't make user2")

	os.Exit(m.Run())
}

func TestUniqueEmails(t *testing.T) {
	d := db.MustGetTestDB()
	_, err := CreateUser(d, "dupe email", "user1@u.com", "pass")
	if err == nil {
		t.Error("duplicate email should have caused an error")
	}
}

func TestCreateReturnsID(t *testing.T) {
	d := db.MustGetTestDB()
	u, err := CreateUser(d, "id check", "id_check@u.com", "idcheck")
	if err != nil {
		t.Error("couldn't create id check user")
	}
	t.Log("created user ID", u.ID)
	if u.ID == 0 {
		t.Error("user ID should not be 0")
	}
}

func TestPasswordAuth(t *testing.T) {
	d := db.MustGetTestDB()
	var err error
	_, err = Authenticate(d, "user1@u.com", "not correct")
	if err == nil {
		t.Error("found user, when we shouldn't have")
	}
	_, err = Authenticate(d, "user1@u.com", "pass")
	if err != nil {
		t.Error("couldn't find user when we should have")
		return
	}
}

func TestCreateStoresLowerCaseEmail(t *testing.T) {
	d := db.MustGetTestDB()
	var err error
	_, err = Authenticate(d, "USER1@U.COM", "pass")
	if err != nil {
		t.Error("auth email should be case insensitive")
	}
}
