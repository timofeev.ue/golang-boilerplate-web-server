package todo

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/incytestudios/golang-boilerplate-web-server/server/db"
)

// GetByID finds an existing Todo by its ID and returns it
func GetByID(d *sqlx.DB, ID int) (*Todo, error) {
	var out Todo
	err := d.Get(&out, `
    SELECT
      id,
      name,
      is_done,
      date_created,
      date_modified
    FROM
      todos
    WHERE
      id=$1
  `, ID)
	return &out, err
}

// DeleteByID deleted a Todo by its ID
func DeleteByID(d *sqlx.DB, ID int) error {
	_, err := d.Exec(`
    DELETE FROM
      todos
    WHERE
      id=$1
  `, ID)
	return err
}

// NewTodo creates a new Todo item and returns the id
func NewTodo(d *sqlx.DB, name string) (*Todo, error) {
	t := &Todo{
		Name:   name,
		IsDone: false,
	}
	row := d.QueryRow(`INSERT INTO todos (name) VALUES ($1) RETURNING ID`, name)
	err := row.Scan(&t.ID)
	return t, err
}

// MGet gets multiple Todos
func MGet(d *sqlx.DB, offset int, limit int, orderBy OrderField,
	orderDir db.OrderDirection) ([]*Todo, error) {
	q := fmt.Sprintf(`
    SELECT
      id,
      name,
      is_done,
      date_created,
      date_modified
    FROM
      todos
    ORDER BY %s %s, name ASC
    OFFSET $1
    LIMIT $2`, orderBy, orderDir)

	todos := []*Todo{}
	err := d.Select(&todos, q, offset, limit)
	return todos, err
}
