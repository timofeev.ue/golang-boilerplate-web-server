package todo

// OrderField represents which column to sort on
type OrderField int

const (
	// OrderByID orders by the model's ID
	OrderByID OrderField = iota
	// OrderByName orders by the model's Name
	OrderByName
	// OrderByDateCreated orders by the model's date created
	OrderByDateCreated
	// OrderByDateModified orders by the model's date modified
	OrderByDateModified
)

func (o OrderField) String() (out string) {
	switch o {
	case OrderByID:
		out = "id"
	case OrderByName:
		out = "name"
	case OrderByDateCreated:
		out = "date_created"
	case OrderByDateModified:
		out = "date_modified"
	}
	return
}
