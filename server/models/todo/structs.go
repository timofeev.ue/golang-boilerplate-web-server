package todo

import "gitlab.com/incytestudios/golang-boilerplate-web-server/server/models"

// Todo represents a "todo" item
type Todo struct {
	Name   string `db:"name" json:"name"`
	IsDone bool   `db:"is_done" json:"is_done"`
	models.AutoFields
}
