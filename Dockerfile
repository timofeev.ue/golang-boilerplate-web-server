FROM golang:1.11-alpine AS build_base

# install any OS deps needed to compile project
RUN apk add bash ca-certificates git gcc g++ libc-dev alpine-sdk
WORKDIR /go/src/gitlab.com/incytestudios/golang-boilerplate-web-server

# force compiler to use module system
ENV GO111MODULE=on
COPY go.mod .
COPY go.sum .

# this should take advantage of the Docker cache and only re-run when the
# mod or sum file changes
RUN go mod download

# this is the builder image, it uses the dependency image from above
# (which hopefully doesn't change much)
FROM build_base AS builder
COPY . .
RUN make static

# Note: scratch image below won't even have bash or ls on it, so if
# you need to get into the resulting # run image and have some tools just
# replace `scratch` with `alpine` below and run `/bin/sh` in the container
# to debug
FROM scratch
COPY templates/ /app/templates/
COPY static/ /app/static/
COPY --from=builder /go/src/gitlab.com/incytestudios/golang-boilerplate-web-server/webserver /app
WORKDIR /app

ENV PORT 5000
ENV DEBUG_LOGGING 1
EXPOSE 5000

CMD ["./webserver"]
