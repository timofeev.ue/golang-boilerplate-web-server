package main

import (
	"gitlab.com/incytestudios/golang-boilerplate-web-server/cmd"
)

func main() {
	cmd.Execute()
}
