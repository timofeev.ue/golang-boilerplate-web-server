package cmd

import (
	"fmt"
	"strconv"

	"github.com/jmoiron/sqlx"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/incytestudios/golang-boilerplate-web-server/server/config"
	"gitlab.com/incytestudios/golang-boilerplate-web-server/server/models/user"
)

func init() {
	rootCmd.AddCommand(userRootCmd)
	userRootCmd.AddCommand(createCmd)
	userRootCmd.AddCommand(passwordCmd)
	createCmd.SetUsageFunc(func(cmd *cobra.Command) error {
		msg := fmt.Sprintf(`Usage:
  %s %s create <email> <password>
`, cmd.Parent().Parent().Use, cmd.Parent().Use)
		cmd.Println(msg)
		return nil
	})
	passwordCmd.SetUsageFunc(func(cmd *cobra.Command) error {
		msg := fmt.Sprintf(`Usage:
  %s %s password <user_id> <new_password>
`, cmd.Parent().Parent().Use, cmd.Parent().Use)
		cmd.Println(msg)
		return nil
	})
}

var userRootCmd = &cobra.Command{
	Use:   "user",
	Short: "user related commands",
	Run: func(cmd *cobra.Command, args []string) {
		cmd.Usage()
	},
}

var createCmd = &cobra.Command{
	Use:   "create",
	Short: "create a new user",
	Long:  "create a user by specifying email and password as positional arguments",
	Args:  cobra.ExactArgs(2),
	Run: func(cmd *cobra.Command, args []string) {
		d := sqlx.MustConnect("postgres", config.DatabaseURL)
		u, err := user.CreateUser(d, args[0], args[0], args[1])
		if err != nil {
			log.Fatal("Error creating user", err)
		}
		log.WithField("id", u.ID).Info("user created")
	},
}

var passwordCmd = &cobra.Command{
	Use:   "password",
	Short: "update password for user",
	Long:  "update password for user indentified by user_id as a positional argument",
	Args:  cobra.ExactArgs(2),
	Run: func(cmd *cobra.Command, args []string) {
		d := sqlx.MustConnect("postgres", config.DatabaseURL)
		id64, err := strconv.ParseInt(args[0], 10, 64)
		if err != nil {
			log.WithFields(log.Fields{
				"id":    args[0],
				"error": err.Error(),
			}).Fatal("invalid user id")
		}
		err = user.SetPassword(d, int(id64), args[1])
		if err != nil {
			log.Fatal("Error setting password", err)
		}
		log.WithField("user_id", id64).Info("password updated")
	},
}
