package cmd

import (
	"fmt"
	"io/ioutil"
	"os"

	"github.com/sebest/logrusly"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/incytestudios/golang-boilerplate-web-server/server/config"
)

var rootCmd = &cobra.Command{
	Use:   "api",
	Short: "A simple API",
	Long:  "Ok I guess it's not that simple",
	Run: func(cmd *cobra.Command, args []string) {
		cmd.Usage()
	},
}

func init() {
	cobra.OnInitialize(initLogging)
}

// Execute will handle the parsing of subcommands and arguments to them
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func initLogging() {
	level := log.InfoLevel
	if config.DebugLogging {
		level = log.DebugLevel
	}
	level = log.TraceLevel
	log.SetLevel(level)

	if config.LogglyToken != "" {
		log.SetFormatter(&log.JSONFormatter{
			TimestampFormat: "2006-01-02 15:04:05",
		})
		log.SetOutput(ioutil.Discard)
		logglyHook := logrusly.NewLogglyHook(config.LogglyToken, "hostname.com", level, "webserver")
		log.AddHook(logglyHook)
	} else {
		log.SetFormatter(&log.TextFormatter{
			// see https://golang.org/src/time/format.go
			TimestampFormat: "2006-01-02 15:04:05",
			FullTimestamp:   true,
			ForceColors:     true,
		})
		log.SetOutput(os.Stdout)
	}
}
