PROJECT_NAME=api
GOCMD=go
GOBUILD=$(GOCMD) build
GOLINT=golint
BINARY_NAME=$(PROJECT_NAME)
DOCKER_NAME=incytestudios/go-static-multistage
SRC=$(shell find . -type f -name '*.go')

.PHONY: all client clean lint test run-reloader static docker tools

all: api

# once go 1.12 comes out we should see if they implemented tool handling yet in the modules code
# for now we need to manually `go get` these in CI and dev
tools:
	$(GOCMD) get github.com/githubnemo/CompileDaemon
	$(GOCMD) get golang.org/x/lint/golint
	$(GOCMD) install github.com/joho/godotenv/cmd/godotenv

clean:
	rm $(BINARY_NAME)

lint:
	$(GOLINT) -set_exit_status ./...
	cd client; npm run lint --fix

test:
	godotenv $(GOCMD) test -v ./server/...

client:
	cd client; npm run build

run-reloader:
# replace the command argument per your platform (on windows it's dirname)
	CompileDaemon \
		-include="*.tmpl" \
		-color=true \
		-log-prefix=false \
		-exclude-dir=.git \
		-exclude-dir=static \
		-build="go build -o $(BINARY_NAME)" \
		-command="./$(BINARY_NAME) serve"
static: clean
	GOARCH=amd64 CGO_ENABLED=0 GOOS=linux go build -o $(BINARY_NAME) .
	strip $(BINARY_NAME)

docker:
	docker build -t $(DOCKER_NAME) .

api: $(SRC)
	$(GOBUILD) -o $(BINARY_NAME) -v
	strip $(BINARY_NAME)
