// Vue asbtracts away most webpack config, this file injects settings
// into the generated webpack.
// to see the actual webpack config `cd client; npm run vue-cli-service inspect`
module.exports = {
  outputDir: "static",
  assetsDir: "assets",
  configureWebpack: {
    resolve: {
      alias: {
        icons: "vue-material-design-icons"
      },
      extensions: [".vue"]
    }
  }
};
