import axios from "axios";
import types from "../../types";

const loginURL = "http://localhost:5000/login";

function getToken() {
  if (window.localStorage) {
    return localStorage.getItem(types.AUTH_TOKEN_KEY) || "";
  }
  return "";
}

function decodeJWT(jwt) {
  const base64URL = jwt.split(".")[1];
  const base64 = base64URL.replace("-", "+").replace("_", "/");
  const out = JSON.parse(window.atob(base64));
  return out;
}

export default {
  // const store = new Vuex.Store({
  namespaced: false,
  state: {
    authToken: getToken(),
    authStatus: "",
    userInfo: {}
  },
  actions: {
    AUTH_REQUEST(context, credentials) {
      context.commit("AUTH_REQUEST");
      return new Promise((resolve, reject) => {
        axios
          .post(loginURL, credentials)
          .then(resp => {
            const {
              data: { token }
            } = resp;
            window.localStorage.setItem(types.AUTH_TOKEN_KEY, token);
            // configure further axios requests to use our token
            axios.defaults.headers.common.Authorization = "Bearer " + token;
            context.commit("AUTH_SUCCESS", token);
            resolve(resp);
          })
          .catch(err => {
            context.commit("AUTH_ERROR");
            window.localStorage.removeItem(types.AUTH_TOKEN_KEY);
            reject(err);
          });
      });
    },
    AUTH_LOGOUT(context) {
      context.commit("AUTH_LOGOUT");
      // TODO: tell server to invalidate the current token?
      localStorage.removeItem(types.AUTH_TOKEN_KEY);
      delete axios.defaults.headers.common.Authorization;
    }
  },
  mutations: {
    AUTH_REQUEST(state) {
      state.status = "loading";
    },
    AUTH_SUCCESS(state, token) {
      state.authStatus = "success";
      state.authToken = token;
      state.status = "";
      state.userInfo = decodeJWT(token);
    },
    AUTH_ERROR(state) {
      state.authStatus = "error";
      state.status = "";
      state.userInfo = {};
    },
    AUTH_LOGOUT(state) {
      state.authStatus = "";
      state.authToken = null;
      state.userInfo = {};
    }
  },
  getters: {
    isLoggedIn: state => !!state.authToken,
    authStatus: state => state.authStatus,
    userInfo: state => state.userInfo
  }
};
