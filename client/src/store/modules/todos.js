import axios from "axios";

const apiRoot = "http://localhost:5000/api/todos";

export default {
  // const store = new Vuex.Store({
  namespaced: false,
  state: {
    status: "",
    todos: []
  },
  actions: {
    REQUEST_TODOS(context) {
      context.commit("API_START");
      return axios
        .get(apiRoot)
        .then(resp => {
          const todos = resp.data;
          // configure further axios requests to use our token
          context.commit("API_SUCCESS");
          context.commit("TODOS_ADDED", todos);
          return resp;
        })
        .catch(err => {
          context.commit("API_ERROR");
          return Promise.reject(err);
        });
    },
    REQUEST_ADD_TODO(context, todo) {
      context.commit("API_START");
      return axios
        .post(apiRoot, todo)
        .then(resp => {
          context.commit("API_SUCCESS");
          context.commit("TODO_ADDED", resp.data);
          return resp;
        })
        .catch(err => {
          context.commit("API_ERROR");
          return Promise.reject(err);
        });
    },
    REQUEST_DELETE_TODO(context, id) {
      context.commit("API_START");
      return axios
        .delete(`${apiRoot}/${id}`)
        .then(resp => {
          context.commit("API_SUCCESS");
          context.commit("TODO_DELETED", id);
          return resp;
        })
        .catch(err => {
          context.commit("API_ERROR");
          return Promise.reject(err);
        });
    }
  },
  mutations: {
    API_START(state) {
      state.status = "loading";
    },
    API_SUCCESS(state) {
      state.status = "";
    },
    API_ERROR(state) {
      state.status = "";
      // maybe? state.todos = [];
    },
    TODO_ADDED(state, todo) {
      state.todos.unshift(todo);
    },
    TODO_DELETED(state, id) {
      var spliceIndex = -1;
      for (var i = 0; i < state.todos.length; i++) {
        const temp = state.todos[i];
        if (temp.id == id) {
          spliceIndex = i;
          break;
        }
      }
      if (spliceIndex != -1) {
        state.todos.splice(spliceIndex, 1);
      }
    },
    TODOS_ADDED(state, todos) {
      state.todos = todos;
    }
  },
  getters: {
    todos: state => state.todos
  }
};
