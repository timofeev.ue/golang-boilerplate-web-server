import Vue from "vue";
import Vuex from "vuex";
import auth from "./modules/auth";
import todos from "./modules/todos";

Vue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    auth,
    todos
  }
});

export default store;
