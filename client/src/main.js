import Vue from "vue";
import Vuex from "vuex";
import Buefy from "buefy";
import Vuelidate from "vuelidate";
import axios from "axios";
import router from "./router";
import App from "./App.vue";
import store from "./store";
import types from "./types";
// see https://www.npmjs.com/package/vue-material-design-icons
import "vue-material-design-icons/styles.css";
import Log from "./plugins/log";

Vue.use(Log, {
  logRoute: "http://localhost:5000/log",
  useConsole: false
});
Vue.use(Vuelidate);
Vue.use(Vuex);
Vue.use(Buefy);

Vue.config.productionTip = process.env.NODE_ENV === "production";

export default new Vue({
  router,
  store,
  created() {
    // prep the axios headers on page reload if the token is there
    this.$log.error("hey girl", {
      fave_number: 12,
      head_size: "big",
      error: "broken",
      undef: undefined,
      nested: {
        x: 10,
        y: 20,
        derp: function() {}
      }
    });
    this.$log.trace("tracing test", { x: 10 });
    this.$log.debug("debug test", { x: 10 });
    this.$log.warn("warning test", { x: 10 });
    this.$log.critical("critical test", { x: 10 });
    const token = localStorage.getItem(types.AUTH_TOKEN_KEY);
    if (token) {
      store.commit("AUTH_SUCCESS", token);
      axios.defaults.headers.common.Authorization = "Bearer " + token;
    }
  },
  render(h) {
    return h(App);
  }
}).$mount("#app");
