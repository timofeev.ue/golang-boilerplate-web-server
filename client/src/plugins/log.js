import axios from "axios";

export default class Log {
  constructor(config = {}) {
    this.config = config;
  }

  static install(Vue, options) {
    Vue.prototype.$log = new Log(options);
  }

  sendData(msg = "", data = {}) {
    data.msg = msg;
    if (this.config.useConsole) {
      if (data.level === "critical") {
        // eslint-disable-next-line no-console
        console.error(msg);
        // eslint-disable-next-line no-console
        return console.error(data);
      }
      // eslint-disable-next-line no-console
      console[data.level](msg);
      // eslint-disable-next-line no-console
      return console[data.level](data);
    }
    return axios.post(this.config.logRoute, data);
  }

  trace(msg = "", data = {}) {
    data.level = "trace";
    return this.sendData(msg, data);
  }

  debug(msg = "", data = {}) {
    data.level = "debug";
    return this.sendData(msg, data);
  }

  info(msg = "", data = {}) {
    data.level = "info";
    return this.sendData(msg, data);
  }

  warn(msg = "", data = {}) {
    data.level = "warning";
    return this.sendData(msg, data);
  }

  error(msg = "", data = {}) {
    data.level = "error";
    return this.sendData(msg, data);
  }

  critical(msg = "", data = {}) {
    data.level = "critical";
    return this.sendData(msg, data);
  }
}
