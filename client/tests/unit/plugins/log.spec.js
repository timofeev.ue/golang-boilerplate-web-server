import Log from "../../../src/plugins/log";

describe("src/util/log.js", () => {
  it("has all the expected methods", () => {
    let log = new Log();
    expect(typeof log.warn).toEqual("function");
  });
});
