module gitlab.com/incytestudios/golang-boilerplate-web-server

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/jmoiron/sqlx v1.2.0
	github.com/joho/godotenv v1.3.0
	github.com/labstack/echo v3.3.8+incompatible
	github.com/labstack/gommon v0.2.8
	github.com/lib/pq v1.0.0
	github.com/sebest/logrusly v0.0.0-20180315190218-3235eccb8edc
	github.com/segmentio/go-loggly v0.5.0 // indirect
	github.com/sirupsen/logrus v1.2.0
	github.com/spf13/cobra v0.0.3
	github.com/spf13/pflag v1.0.3 // indirect
	google.golang.org/appengine v1.3.0 // indirect
)
