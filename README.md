# Golang Boilerplate Web Server

A decent starting system of a go webserver with server-side templates and a basic project makefile

## Installing golang 1.11

[Ubuntu](https://github.com/golang/go/wiki/Ubuntu)

TODO: Mac

TODO: Windows

After install for Ubuntu:
Add to you path: 

`PATH=$PATH:/usr/lib/go-1.11/bin:/<HOME DIR>/go/bin`

## Code Checkout / Setup
Since go v1.11 this source code can be checked out whereever you like. See a basic example below

```bash
mkdir ~/code
cd ~/code
git clone git@gitlab.com:incytestudios/golang-boilerplate-web-server.git
cd golang-boilerplate-web-server
cp .env.example .env
```

## DB Setup
This project uses Postgres v11. There is a provided docker config that the server will be able to use. Set it up like so

```bash
docker-compose up postgres
# switch to another shell
make api
./api migrate -d # dry run doesn't modify the db but verifies connection and migration files
./api migrate # run the actual migrations on the main db
./api migrate -t # run the actual migrations on the test database
make test
```

**NOTE**: If you get errors on the dry run, you most likely have a misconfigured `.env` file. So ensure the `DATABASE_URL` matches
What you have in `docker-compose.yaml`

## Logging

The server is setup to log to STDOUT by default. If the env variable `LOGGLY_TOKEN` is set to a valid loggly "customer token" then the server will forward JSON logs to Loggly. This is off by default

## Running Development Mode

In development mode you'll need to have 3 things running, it's recommended to start them in different shell sessions

  - Postgres
  - client: the Vue fontend
  - api: the Go backend

### Starting up Postgres via docker-compose

You most likely already did this step above while setting up unit tests. The following command will start postgres in docker for you
listening on `:5432`. The see the credentials and main database name, see `docker-compose.yaml`

```base
docker-compose up postgres
```

### Starting up the client (Vue frontend)

This command will start Vue's server which offers hot reloading of JS/CSS/HTML/Vue files and is good to leave running while working on front-end code

```bash
cd client; npm run serve
```

### Starting up the api (Go backend)

We use [CompileDaemon](https://github.com/githubnemo/CompileDaemon) for hot reloading the server during development.

```bash
make run-reloader
```

## Production Setup

In production, the go API will serve its own routes as well as the static version of the client app. This can be simulated locally like so...

```bash
make client
make api
./api serve
<open browser to localhost:5000>
```

## Core Libraries

https://echo.labstack.com/guide/installation
